; .global makes the symbol visible to the linker, so that
;   it can be called from our C code. In comparison, you
;   would not be able to call e.g. `exp_zero`, even if you
;   specified it as `extern` in your C file.
.global _my_pow_asm

; p2 stands for power of two. This instructs the assembler to
;   align all functions at 2^x boundaries. Here we specify 2, so
;   the functions will be 4-byte aligned. This is a requirement
;   from the CPU architecture.
.p2align 2

; Tells the assembler to switch to the "text" segment, which
;   is where the code goes.
.text

; This is the start of our function. The leading underscore
; appears to be a linker symbol naming standard.
_my_pow_asm:
                        ; w0 is our base
    cmp w0, #0          ; base less than (lt) 0?
    blt invalid_args    ; if so, go to invalid_args

                        ; w1 is our exponent
    cmp w1, #0          ; exponent < 0?
    blt invalid_args    ; if so, go to invalid_args


    beq exp_zero        ; exponent == 0?
                        ; if so, go to exp_zero

    mov w3, w0          ; copy our base to w3,
                        ; we'll need it soon, since
                        ; we'll be modifying w0

loop:
    mul w0, w3, w0      ; result = base (w3) * result (w0)
    sub w1, w1, 1       ; exponent -= 1

    cmp w1, #1          ; exponent > 1?
    bgt loop            ; if so, we continue looping

    ret                 ; return from function

exp_zero:
    mov x0, #1          ; x^0 is always 1
    ret                 ; return from function

invalid_args:
    mov w0, #-1         ; on error we return -1
    ret                 ; return from function
