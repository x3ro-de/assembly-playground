#include <stdio.h>
#include <stdint.h>

extern int my_pow_asm(int32_t base, int32_t exp);

int main(int argc, char const *argv[]) {
    int32_t result = my_pow_asm(2, 8);
    printf("%d\n", result);
    return 0;
}
